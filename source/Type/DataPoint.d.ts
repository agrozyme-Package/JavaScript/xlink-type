export default interface DataPoint {
  index: number;
  value: any;
}
