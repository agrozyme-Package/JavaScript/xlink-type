interface ErrorResponse {
  error: {
    msg: string; code: number;
  };
}

export default ErrorResponse;
