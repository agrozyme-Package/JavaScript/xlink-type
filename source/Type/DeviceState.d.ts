import DataPoint from './DataPoint';

export default interface DeviceState {
  can_be_found: boolean;
  datapoint: DataPoint[];
  id: string;
  name: string;
}
