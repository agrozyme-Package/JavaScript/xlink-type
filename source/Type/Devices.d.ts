import Device from './Device';

export interface DevicesError {
  error?: { msg: string };
}

type Devices = DevicesError | Device[];

export default Devices;
